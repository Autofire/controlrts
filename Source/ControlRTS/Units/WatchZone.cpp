// Fill out your copyright notice in the Description page of Project Settings.

#include "WatchZone.h"

#include "Components/PrimitiveComponent.h"
#include "GameFramework/Actor.h"
#include "Engine/EngineTypes.h"


UWatchZone::UWatchZone() {
	DebugEvents = false;
}

void UWatchZone::BeginPlay() {
	Super::BeginPlay();

	OnComponentBeginOverlap.AddDynamic(this, &UWatchZone::RecieveBeginOverlapEvent);
	OnComponentEndOverlap.AddDynamic(this, &UWatchZone::RecieveEndOverlapEvent);
}

void UWatchZone::RecieveBeginOverlapEvent(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (IsActorWatchedFor(OverlappedComponent, OtherActor, OtherComp, OtherBodyIndex, bFromSweep, SweepResult)) {

		VisibleActors.AddUnique(OtherActor);

		PrintVisibleActors();
	}
}

void UWatchZone::RecieveEndOverlapEvent(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	// We don't even need to check if it's in there before hand or if it's something that's visible;
	// chances are, it might not be if we have some kind of raycast check.
	VisibleActors.Remove(OtherActor);

	PrintVisibleActors();
}


bool UWatchZone::IsActorWatchedFor_Implementation(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) const
{
	// By default, we accept everything.
	return true;
}

void UWatchZone::PrintVisibleActors()
{

	if (DebugEvents) {
		if (VisibleActors.Num() == 0) {
			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, "No actors are visible");
		}
		else {

			FString DebugStr;
			for (AActor* VA : VisibleActors) {
				DebugStr += VA->GetName();
				DebugStr += " ";
			}

			GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Red, DebugStr);
		}
	}
}
