// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/SphereComponent.h"
#include "WatchZone.generated.h"

/**
 * Watches for other objects, keeping a list of everything that is
 * currently overlapping with us. By default, it will track every
 * actor that it comes across, but children can place restrictions.
 *
 * Note that, though the list of overlapped components is already
 * tracked for us, we also want to keep track of the ORDER that we
 * spot them. It's easier to track it ourselves.
 */
UCLASS(Blueprintable)
class CONTROLRTS_API UWatchZone : public USphereComponent
{
	GENERATED_BODY()
	
	// TODO We don't have any way of signalling when new things are added.
	// TODO We also can't ask for the existing list of visible characters.

public:
	UWatchZone();

	void BeginPlay() override;

protected:
	/**
	 *  This controls what gets added to the list of objects.
	 *  This is only garunteed to be called when we start overlapping the actor,
	 *  so don't include code that may change its answer between overlaps (i.e. a raycast),
	 *  since a change in the result won't get the object magically added to the list!
	 */
	UFUNCTION(BlueprintNativeEvent, BlueprintPure, Category = "Vision")
	bool IsActorWatchedFor(class UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const struct FHitResult& SweepResult) const;

	virtual bool IsActorWatchedFor_Implementation(class UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const struct FHitResult& SweepResult) const;

private:
	TArray<class AActor*> VisibleActors;

	UFUNCTION()
	void RecieveBeginOverlapEvent(class UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const struct FHitResult& SweepResult);

	UFUNCTION()
	void RecieveEndOverlapEvent(class UPrimitiveComponent* OverlappedComponent, class AActor* OtherActor, class UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	// Debugging functions
private:
	UPROPERTY(EditAnywhere, Category = "Vision", AdvancedDisplay)
	bool DebugEvents;

	void PrintVisibleActors();

};
