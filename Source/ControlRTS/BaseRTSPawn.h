// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Pawn.h"
#include "BaseRTSPawn.generated.h"

/**
 * 
 */
UCLASS()
class CONTROLRTS_API ABaseRTSPawn : public APawn
{
	GENERATED_BODY()

public:
	ABaseRTSPawn();

	// Called every frame.
	virtual void Tick(float DeltaSeconds) override;

	/** Draws the highlight that goes with the given trace */
	UFUNCTION(BlueprintCallable, Category = Cursor )
	void UpdateGroundCursor(FHitResult TraceHitResult);

	/** Returns CursorToWorld subobject **/
	FORCEINLINE class UDecalComponent* GetCursorToWorld() { return CursorToWorld; }

public:
	//UFUNCTION(BlueprintCallable, Category = Maintainence )
	//void UpdateCursorPosition();

private:
	/** A decal that projects to the cursor location. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UDecalComponent* CursorToWorld;
};
