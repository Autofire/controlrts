// Fill out your copyright notice in the Description page of Project Settings.

#include "BaseRTSPlayerController.h"

ABaseRTSPlayerController::ABaseRTSPlayerController() {
	EdgeScrollAreaWidth = 10.f;

	PrimaryActorTick.bCanEverTick = true;
	//PrimaryActorTick.bStartWithTickEnabled = true;
}

FVector ABaseRTSPlayerController::GetEdgeScrollInput(FVector2D MousePosition, FVector2D ViewportSize) const {
	FVector result = FVector::ZeroVector;

	if (MousePosition.X < EdgeScrollAreaWidth) {
		result.Y = -1.f;
	}
	else if (MousePosition.X > ViewportSize.X - EdgeScrollAreaWidth) {
		result.Y = 1.f;
	}

	if (MousePosition.Y < EdgeScrollAreaWidth) {
		result.X = 1.f;
	}
	else if (MousePosition.Y > ViewportSize.Y - EdgeScrollAreaWidth) {
		result.X = -1.f;
	}

	return result;
}
