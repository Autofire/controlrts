// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "BaseRTSPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class CONTROLRTS_API ABaseRTSPlayerController : public APlayerController
{
	GENERATED_BODY()

public:
	ABaseRTSPlayerController();

	UPROPERTY(EditAnywhere, Category = Mouse)
	float EdgeScrollAreaWidth;
	
public:
	UFUNCTION(BlueprintCallable, BlueprintPure, Category = Mouse)
	FVector GetEdgeScrollInput(FVector2D MousePosition, FVector2D ViewportSize) const;
};
