// Copyright Epic Games, Inc. All Rights Reserved.

#include "ControlRTS.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, ControlRTS, "ControlRTS" );

DEFINE_LOG_CATEGORY(LogControlRTS)
 