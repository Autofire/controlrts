// Fill out your copyright notice in the Description page of Project Settings.


#include "BaseRTSPawn.h"

#include "Components/SceneComponent.h"
#include "Components/DecalComponent.h"
#include "Materials/Material.h"

ABaseRTSPawn::ABaseRTSPawn() {

	// Activate ticking in order to update the cursor every frame.
	PrimaryActorTick.bCanEverTick = true;
	//PrimaryActorTick.bStartWithTickEnabled = true;

	// Set up root component
	USceneComponent* newRoot = CreateDefaultSubobject<USceneComponent>("RTSPawnRoot");
	SetRootComponent(newRoot);

	// Create a decal in the world to show the cursor's location
	CursorToWorld = CreateDefaultSubobject<UDecalComponent>("CursorToWorld");
	CursorToWorld->SetupAttachment(RootComponent);
	static ConstructorHelpers::FObjectFinder<UMaterial> DecalMaterialAsset(TEXT("Material'/Game/TopDownCPP/Blueprints/M_Cursor_Decal.M_Cursor_Decal'"));
	if (DecalMaterialAsset.Succeeded())
	{
		CursorToWorld->SetDecalMaterial(DecalMaterialAsset.Object);
	}
	CursorToWorld->DecalSize = FVector(16.0f, 32.0f, 32.0f);
	CursorToWorld->SetRelativeRotation(FRotator(90.0f, 0.0f, 0.0f).Quaternion());
}

void ABaseRTSPawn::Tick(float DeltaSeconds)
{
	Super::Tick(DeltaSeconds);

	//BaseTick(DeltaSeconds);
}

void ABaseRTSPawn::UpdateGroundCursor(FHitResult TraceHitResult) {
	if (CursorToWorld != nullptr) {
		FVector CursorFV = TraceHitResult.ImpactNormal;
		FRotator CursorR = CursorFV.Rotation();
		CursorToWorld->SetWorldLocation(TraceHitResult.Location);
		CursorToWorld->SetWorldRotation(CursorR);
	}
}
