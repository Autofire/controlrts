// Copyright Epic Games, Inc. All Rights Reserved.

#include "ControlRTSGameMode.h"
#include "ControlRTSPlayerController.h"
#include "ControlRTSCharacter.h"
#include "UObject/ConstructorHelpers.h"

AControlRTSGameMode::AControlRTSGameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AControlRTSPlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/TopDownCPP/Blueprints/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}